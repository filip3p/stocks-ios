//
//  StocksService.swift
//  Stocks
//
//  Created by Filip Stojanovski on 14.2.22.
//

import Foundation
import Alamofire

struct StocksService {
    
    static func getStocks(_ marketCapMoreThan: Int,
                          _ volumeMoreThan: Int,
                          _ sector: String,
                          _ exchange: String,
                          _ limit: Int,
                          then completion:@escaping (Result<[Stock], AFError>) -> Void) {

        let url = urlBuilder.url(for: .stocks)

        let params: Parameters = ["marketCapMoreThan": marketCapMoreThan,
                                  "volumeMoreThan": volumeMoreThan,
                                  "sector": sector,
                                  "exchange": exchange,
                                  "limit": limit,
                                  "apikey": Constants.apiKey
        ]
        
        AF.request(url, parameters: params).response { response in
            if let error = response.error {
                completion(.failure(error))
            } else if let jsonData = response.data {

                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    
                    let stocks = try decoder.decode([Stock].self, from: jsonData)
                    completion(.success(stocks))
                } catch {
                    completion(.failure(AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))))
                }

            }
        }
    }
}
