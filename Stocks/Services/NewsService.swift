//
//  NewsService.swift
//  Stocks
//
//  Created by Filip Stojanovski on 14.2.22.
//

import Foundation
import Alamofire

struct NewsService {
    
    static func getNews(_ tickers: String,
                        _ limit: Int,
                        then completion:@escaping (Result<[News], AFError>) -> Void) {

        let url = urlBuilder.url(for: .news)

        let params: Parameters = ["tickers": tickers,
                                  "limit": limit,
                                  "apikey": Constants.apiKey
        ]
        
        AF.request(url, parameters: params).response { response in
            if let error = response.error {
                completion(.failure(error))
            } else if let jsonData = response.data {
                do {
                    let decoder = JSONDecoder()
                    //decoder.dateDecodingStrategy = .iso8601
                    
                    let stocks = try decoder.decode([News].self, from: jsonData)
                    completion(.success(stocks))
                } catch {
                    completion(.failure(AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))))
                }

            }
        }
    }
}
