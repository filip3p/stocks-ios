//
//  PhoneNumberCountryCode.swift
//  Stocks
//
//  Created by Filip Stojanovski on 18.2.22.
//

import Foundation

struct PhoneNumberCountryCode: Codable {

    enum CodingKeys: String, CodingKey {
        case countryName = "name"
        case countryCode = "iso_2code"
        case dialCode = "dial_code"
        case iso3 = "iso_3code"
    }

    let countryCode: String?
    let countryName: String
    let dialCode: String?
    var iso3: String?
}
