//
//  Constants.swift
//  Stocks
//
//  Created by Filip Stojanovski on 14.2.22.
//

import Foundation

enum Constants {
    
    static let baseAPIURL = "https://financialmodelingprep.com/api/"
    static let apiVersion = "v3"
    static let apiKey = "d64d179adb2bcbf73edd76abd7e9e477"
    
    enum ErrorDomain {
        static let internalError = "com.virginmobile.internalError"
    }
    
}
