//
//  StocksCellConfigurator.swift
//  Stocks
//
//  Created by Filip Stojanovski on 14.2.22.
//

import UIKit

final class StocksCellConfigurator {
    
    // MARK: - Private Properties
    private weak var tableView: UITableView?
    private weak var delegate: StocksViewController?
    
    // MARK: - Lifecycle
    init(tableView: UITableView,
         delegate: StocksViewController) {
        
        self.tableView = tableView
        self.delegate = delegate
        registerCell()
    }
    
    // MARK: - Cell Configuration
    func registerCell() {
        
        guard let tableView = self.tableView else {
            return
        }
        tableView.register(UINib(nibName: "StockTableViewCell", bundle: nil), forCellReuseIdentifier: "stockCell")
    }
    
    func configureCell(
        indexPath: IndexPath,
        stock: Stock,
        isAddButtonVisible: Bool,
        isMyStock: Bool) -> StockTableViewCell {
            
            guard let tableView = self.tableView else {
                fatalError("Unable to configure cell, table view is nil")
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "stockCell", for: indexPath) as! StockTableViewCell
            
            cell.delegate = delegate
            cell.stock = stock
            setup(
                cell: cell,
                indexPath: indexPath,
                stock: stock,
                isAddButtonVisible: isAddButtonVisible,
            isMyStock: isMyStock)
            
            return cell
        }
    
    func setup(
        cell: StockTableViewCell,
        indexPath: IndexPath,
        stock: Stock,
        isAddButtonVisible: Bool,
        isMyStock: Bool) {
            
            cell.stockNameLabel.text = stock.companyName
            if let mCap = stock.marketCap {
                cell.marketCapLabel.text = "Market Cap: \(mCap.formattedWithSeparator)"
            }
            cell.addButton.isHidden = !isAddButtonVisible
            cell.addButton.isEnabled = !isMyStock
        }
}
