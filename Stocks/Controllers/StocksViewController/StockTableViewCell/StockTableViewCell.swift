//
//  StockTableViewCell.swift
//  Stocks
//
//  Created by Filip Stojanovski on 14.2.22.
//

import Foundation
import UIKit

protocol StockTableViewCellDelegate: AnyObject {
    func didTapAddStock(_ : Stock, cell: StockTableViewCell)
}

class StockTableViewCell: UITableViewCell {
    
    weak var delegate: StockTableViewCellDelegate?
    var stock: Stock?
    
    @IBOutlet var stockNameLabel: UILabel!
    @IBOutlet weak var marketCapLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func addStockWasTapped(_ sender: Any) {
        if let stock = stock {
            delegate?.didTapAddStock(stock, cell: self)
        }
    }
    
}
