//
//  StocksViewController.swift
//  Stocks
//
//  Created by Filip Stojanovski on 13.2.22.
//

import UIKit
import Alamofire
import CoreData

class StocksViewController: UIViewController {
    
    // MARK: - Private properties
    private let viewModel = StocksViewModel()
    private var isFiltering: Bool = false {
        didSet {
            search()
        }
    }
    private var stocks: [Stock] = []
    private var filteredStocks: [Stock] = []
    private var dataSource: [Stock] {
        return isFiltering ? filteredStocks : stocks
    }
    private lazy var stockCellConfigurator: StocksCellConfigurator = {
        StocksCellConfigurator(tableView: stocksTableView, delegate: self)
    }()
    
    // MARK: - Properties
    var context: NSManagedObjectContext?
    
    // MARK: - IBOutlets
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet private weak var clearTextFieldButton: UIButton!
    @IBOutlet weak var stocksTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.delegate = self
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showSpinner(onView: self.view)
        viewModel.fetchMyStocks()
        viewModel.fetchStocks()
    }
    
    // MARK: - IBActions
    @IBAction func didTapClearButton(_ sender: Any) {
        
        clearTextFieldButton.isHidden = true
        searchTextField.text = nil
        /// With setting false we are calling search() function
        isFiltering = false
    }
    
    @IBAction func didTapOrderByButton(_ sender: Any) {
        
        showOrderBy()
    }
    
    @IBAction func showCountriesPopup(_ sender: Any) {
        
        let vc = CountriesViewController(delegate: self)
        present(vc, animated: true, completion: nil)
    }
}

private extension StocksViewController {
    
    private func search() {
        
        if let searchText = searchTextField.text, !searchText.isEmpty {
            filteredStocks = stocks.filter {
                ($0.companyName ?? "").lowercased().contains(searchText.lowercased())
            }
        }
        stocksTableView.reloadData()
    }
    
    private func showOrderBy() {
        
        let alert = UIAlertController(title: "Order By", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "A-Z", style: .default , handler:{ (UIAlertAction)in
            self.isFiltering = false
            self.stocks.sort { $0.companyName?.lowercased() ?? "" < $1.companyName?.lowercased() ?? "" }
            self.stocksTableView.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: "Market Cap", style: .default , handler:{ (UIAlertAction)in
            self.isFiltering = false
            self.stocks.sort { $0.marketCap ?? 0 > $1.marketCap ?? 0 }
            self.stocksTableView.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource
extension StocksViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        dataSource.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return stockCellConfigurator.configureCell(
            indexPath: indexPath,
            stock: dataSource[indexPath.row],
            isAddButtonVisible: true,
            isMyStock: viewModel.myStocks.contains(dataSource[indexPath.row].companyName ?? ""))
    }
}

extension StocksViewController: StocksViewModelDelegate {
    
    func onUpdateData(_ stocks: [Stock]) {
        
        self.stocks = stocks
        removeSpinner()
        stocksTableView.reloadData()
    }
    
    func onError(_ err: AFError) {
        
        removeSpinner()
        show(error: err)
    }
}

extension StocksViewController: StockTableViewCellDelegate {
    
    func setupUI() {
        
        stocksTableView.estimatedRowHeight = 68
        stocksTableView.delegate = self
        stocksTableView.dataSource = self
        
        searchTextField.returnKeyType = .go
        searchTextField.autocorrectionType = .no
        searchTextField.delegate = self
        searchTextField.addTarget(self, action: #selector(searchTextDidChange), for: .editingChanged)
        searchTextField.placeholder = "Search stocks"
        clearTextFieldButton.isHidden = true
    }
    
    func getContext() {
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if #available(iOS 10.0, *) {
            self.context = appDelegate?.persistentContainer.viewContext
        } else {
            self.context = appDelegate?.managedObjectContext
        }
    }
    
    func didTapAddStock(_ stock: Stock, cell: StockTableViewCell) {
        self.getContext()
        let model = NSEntityDescription.insertNewObject(forEntityName: "MyStocks", into: self.context!) as! MyStocks
        model.company_name = stock.companyName
        model.stock_ticker_symbol = stock.symbol
        cell.addButton.isEnabled = false
        do {
            try self.context?.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
}

extension StocksViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    @objc func searchTextDidChange(_ sender: UITextField) {
        guard let searchText = searchTextField.text, !searchText.isEmpty else {
            clearTextFieldButton.isHidden = true
            isFiltering = false
            return
        }
        clearTextFieldButton.isHidden = false
        isFiltering = true
    }
}

extension StocksViewController: CountriesViewControllerDelegate {
    
    func didSelect(countries: [PhoneNumberCountryCode]) {
        if !countries.isEmpty {
            self.isFiltering = false
            let stocksFilteredByCountries = stocks.filter { stock in
                if countries.filter({ $0.countryCode == stock.country }).isEmpty {
                    return false
                } else {
                    return true
                }
            }
            stocks = stocksFilteredByCountries
            stocksTableView.reloadData()
        } else {
            self.isFiltering = false
            viewModel.fetchStocks()
        }
    }
}

