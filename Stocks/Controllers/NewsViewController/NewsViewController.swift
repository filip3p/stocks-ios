//
//  NewsViewController.swift
//  Stocks
//
//  Created by Filip Stojanovski on 16.2.22.
//

import Foundation
import UIKit
import Alamofire

class NewsViewController: UIViewController {
    
    // MARK: - Private properties
    private let symbol: String
    private let viewModel = NewsViewModel()
    private var news: [News] = []
    private lazy var newsCellConfigurator: NewsCellConfigurator = {
        NewsCellConfigurator(tableView: newsTableView, delegate: self)
    }()
    
    // MARK: - IBOutlets
    @IBOutlet weak var newsTableView: UITableView!
    
    // MARK: - Init
    init(symbol: String) {
        self.symbol = symbol
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.delegate = self
        
        newsTableView.delegate = self
        newsTableView.dataSource = self
        newsTableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "newsCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showSpinner(onView: self.view)
        viewModel.fetchNews(for: symbol)
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource
extension NewsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        news.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return newsCellConfigurator.configureCell(
            indexPath: indexPath,
            news: news[indexPath.row])
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if let url = URL(string: news[indexPath.row].url ?? "") {
            UIApplication.shared.open(url)
        }
    }
}

extension NewsViewController: NewsViewModelDelegate {
    
    func onUpdateData(_ news: [News]) {
        
        self.news = stocks
        self.removeSpinner()
        newsTableView.reloadData()
    }
    
    func onError(_ err: AFError) {
        
        self.removeSpinner()
        show(error: err)
    }
}
