//
//  NewsCellConfigurator.swift
//  Stocks
//
//  Created by Filip Stojanovski on 17.2.22.
//

import UIKit

final class NewsCellConfigurator {
    
    // MARK: - Private Properties
    private weak var tableView: UITableView?
    private weak var delegate: NewsViewController?
    
    // MARK: - Lifecycle
    init(tableView: UITableView, delegate: NewsViewController) {
        
        self.tableView = tableView
        self.delegate = delegate
        registerCell()
    }
    
    // MARK: - Cell Configuration
    func registerCell() {
        
        guard let tableView = self.tableView else {
            return
        }
        tableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "newsCell")
    }
    
    func configureCell(
        indexPath: IndexPath,
        news: News) -> NewsTableViewCell {
            
            guard let tableView = self.tableView else {
                fatalError("Unable to configure cell, table view is nil")
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! NewsTableViewCell
            
            setup(
                cell: cell,
                indexPath: indexPath,
                news: news)
            
            return cell
        }
    
    func setup(
        cell: NewsTableViewCell,
        indexPath: IndexPath,
        news: News) {
            
            cell.titleLabel.text = news.title
            cell.newsLabel.text = news.text
            cell.newsImageView.loadImageUsingUrl(stringUrl: news.image ?? "")
            cell.siteLabel.text = news.site
            
            if let date = news.publishedDate {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM-dd-yyyy"
                cell.dateLabel.text = dateFormatter.string(for: dateFormatter.date(from: date))
            }
        }
}
