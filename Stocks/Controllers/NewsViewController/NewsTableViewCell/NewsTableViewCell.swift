//
//  NewsTableViewCell.swift
//  Stocks
//
//  Created by Filip Stojanovski on 17.2.22.
//

import UIKit


class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newsLabel: UILabel!
    @IBOutlet weak var siteLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
}
