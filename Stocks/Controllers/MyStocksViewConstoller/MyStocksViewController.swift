//
//  MyStocksViewController.swift
//  Stocks
//
//  Created by Filip Stojanovski on 16.2.22.
//

import Foundation
import UIKit
import CoreData

class MyStocksViewController: UIViewController {
    
    // MARK: - Properties
    var stocks: [NSManagedObject] = []
    
    // MARK: - IBOutlets
    @IBOutlet weak var myStocksTableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myStocksTableView.delegate = self
        myStocksTableView.dataSource = self
        myStocksTableView.register(UINib(nibName: "StockTableViewCell", bundle: nil), forCellReuseIdentifier: "stockCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        noDataLabel.isHidden = true
        myStocksTableView.isHidden = true
        
        populateData()
    }
}

// MARK: - Private extension
private extension MyStocksViewController {
    
    func populateData() {
        
        guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
        
        let managedContext =
        appDelegate.persistentContainer.viewContext
        
        let fetchRequest =
        NSFetchRequest<NSManagedObject>(entityName: "MyStocks")
        
        do {
            stocks = try managedContext.fetch(fetchRequest)
            if stocks.isEmpty {
                noDataLabel.isHidden = false
            } else {
                myStocksTableView.isHidden = false
                myStocksTableView.reloadData()
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource
extension MyStocksViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let stock = stocks[indexPath.row]
        let cell =
        tableView.dequeueReusableCell(withIdentifier: "stockCell",
                                      for: indexPath) as! StockTableViewCell
        cell.stockNameLabel.text = stock.value(forKeyPath: "company_name") as? String
        cell.addButton.isHidden = true
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return stocks.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = NewsViewController(symbol: stocks[indexPath.row].value(forKeyPath: "stock_ticker_symbol") as? String ?? "")
        present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            guard let appDelegate =
                    UIApplication.shared.delegate as? AppDelegate else {
                        return
                    }
            
            let context = appDelegate.persistentContainer.viewContext
            
            let object = stocks[indexPath.row]
            context.delete(object)
            
            do {
                try context.save()
            } catch
                let error as NSError {
                print("Could not save. \(error),\(error.userInfo)")
            }
            stocks.remove(at: indexPath.row)
            myStocksTableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}
