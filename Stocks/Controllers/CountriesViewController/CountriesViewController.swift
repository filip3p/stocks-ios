//
//  CountriesViewController.swift
//  Stocks
//
//  Created by Filip Stojanovski on 18.2.22.
//

import Foundation
import UIKit

protocol CountriesViewControllerDelegate: AnyObject {
    func didSelect(countries: [PhoneNumberCountryCode])
}

class CountriesViewController: UIViewController {
    
    var countries: [PhoneNumberCountryCode] = []
    
    weak var delegate: CountriesViewControllerDelegate?
    
    // MARK: - IBOutlets
    @IBOutlet weak var countriesTableView: UITableView!
    
    init(delegate: CountriesViewControllerDelegate) {
        
        self.delegate = delegate
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.countries = CountryUtil.countries
        
        countriesTableView.delegate = self
        countriesTableView.dataSource = self
        countriesTableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        let selectedRows = countriesTableView.indexPathsForSelectedRows
        let selectedCountries = selectedRows?.map { countries[$0.row] }
        
        delegate?.didSelect(countries: selectedCountries ?? [])
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource
extension CountriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        countries.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = countries[indexPath.row].countryName
        
        let selectedIndexPaths = tableView.indexPathsForSelectedRows
        let rowIsSelected = selectedIndexPaths != nil && selectedIndexPaths!.contains(indexPath)
        cell.accessoryType = rowIsSelected ? .checkmark : .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        cell.accessoryType = .checkmark
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        cell.accessoryType = .none
    }
}
