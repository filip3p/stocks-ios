//
//  NewsViewModel.swift
//  Stocks
//
//  Created by Filip Stojanovski on 17.2.22.
//

import Foundation
import Alamofire

protocol NewsViewModelDelegate: AnyObject {
    
    func onUpdateData(_ news: [News])
    func onError(_ err: AFError)
}

final class NewsViewModel {
    
    weak var delegate: NewsViewModelDelegate?
    
    func fetchNews(for symbol: String) {
        
        NewsService.getNews(symbol,
                            100,
                            then: { result in
            switch result {
            case let .success(news):
                self.delegate?.onUpdateData(news)
            case let .failure(err):
                self.delegate?.onError(err)
            }
        })
    }
}
