//
//  StocksViewModel.swift
//  Stocks
//
//  Created by Filip Stojanovski on 14.2.22.
//

import Foundation
import Alamofire
import CoreData

protocol StocksViewModelDelegate: AnyObject {
    
    func onUpdateData(_ stocks: [Stock])
    func onError(_ err: AFError)
}

final class StocksViewModel {
    
    weak var delegate: StocksViewModelDelegate?
    
    var myStocks: [String] = []
    
    func fetchStocks() {
        
        StocksService.getStocks(1000000000,
                                10000,
                                "Technology",
                                "NASDAQ",
                                100,
                                then: { result in
            switch result {
            case let .success(stocks):
                self.delegate?.onUpdateData(stocks)
            case let .failure(err):
                self.delegate?.onError(err)
            }
        })
    }
    
    func fetchMyStocks() {
        
        guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
        
        let managedContext =
        appDelegate.persistentContainer.viewContext
        
        let fetchRequest =
        NSFetchRequest<NSManagedObject>(entityName: "MyStocks")
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            results.forEach { result in
                if let name = result.value(forKey: "company_name") as? String {
                    myStocks.append(name)
                }
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}
