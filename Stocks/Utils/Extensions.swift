//
//  Extensions.swift
//  Stocks
//
//  Created by Filip Stojanovski on 17.2.22.
//

import Foundation
import UIKit
import Alamofire

let imageCache = NSCache<NSString, AnyObject>()

extension UIImageView {
    
    func loadImageUsingUrl(stringUrl : String) {
        
        guard let url = URL(string: stringUrl) else {
            return
        }
        
        if let imageFromCache = imageCache.object(forKey: stringUrl as NSString) as? UIImage {
            self.image = imageFromCache
            return
        }
        
        let dataTask: URLSessionDataTask =
        URLSession(configuration: .default).dataTask(with: url) { [weak self] data, response, error in
            
            if let error = error {
                print(error)
                return
            } else if
                let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                
                DispatchQueue.main.async {
                    
                    let imageToCache = UIImage(data: data)
                    
                    imageCache.setObject(imageToCache!, forKey: stringUrl as NSString)
                    self?.image = imageToCache
                }
            }
        }
        dataTask.resume()
    }
}

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = " "
        return formatter
    }()
}

extension Numeric {
    var formattedWithSeparator: String { Formatter.withSeparator.string(for: self) ?? "" }
}

var vSpinner : UIView?

extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
    
    func show(error: AFError) {
        
        let alert = UIAlertController(title: "Error", message: "Something went wrong \(error.localizedDescription)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
