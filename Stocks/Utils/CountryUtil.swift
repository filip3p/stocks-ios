//
//  CountryUtil.swift
//  Stocks
//
//  Created by Filip Stojanovski on 18.2.22.
//

import UIKit

class CountryUtil {
    
    static var countries: [PhoneNumberCountryCode] {
        guard let url = Bundle.main.url(forResource: "countries", withExtension: "json") else { return [] }
        do {
            let jsonData = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let results = try decoder.decode([PhoneNumberCountryCode].self, from: jsonData)
            return results
        }
        catch {
            return []
        }
    }
}
