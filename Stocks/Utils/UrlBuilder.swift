//
//  UrlBuilder.swift
//  Stocks
//
//  Created by Filip Stojanovski on 14.2.22.
//

import Foundation

// MARK: - APIURL
enum APIURL {
    
    case stocks
    case news
    
    fileprivate var path: String {
        switch self {
        case .stocks:
            return "stock-screener"
        case .news:
            return "stock_news"
        }
    }
}

// MARK: - URLBuilder
final class URLBuilder {
    
    func url(for apiURL: APIURL) -> String {
        
        "\(Constants.baseAPIURL)\(Constants.apiVersion)/\(apiURL.path)"
    }
}
